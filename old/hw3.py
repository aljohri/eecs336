import math

A = [2, 9, 15, 22, 24]
B = [1, 4, 5, 7, 18, 22, 29]

m = len(A)
n = len(B)

i = (m-1)/2 # these are floored
j = (n-1)/2 # 

def lowerme (iORj):
	''' do the appropriate thing to find the lower index of i or j'''
	if iORj > 2:
		return iORj/2
	elif iORj == 2:
		return 1
	elif iORj == 1:
		return 0
	else: 
		return -1

def findKthSmallest (A, B, i, j, k):
	print "boop " #+ str(A[i]) + " " + str(B[j]) + " i=" + str(i) + " j=" +str(j)
	if k > len(A) + len(B):
		print "K is Too Big"
		return -1

	smallest_index_thusfar = i + j + 2
	print "Smallest Index Now = " + str(smallest_index_thusfar)
	print "k = " + str(k)

	if i == 0 and j == 0 and k == 2:
		return min(A[i], B[j])

	if A[i] > B[j]:
		if smallest_index_thusfar == k:
			print "A"
			return A[i]
		
		elif smallest_index_thusfar < k:
			print "B"
			return findKthSmallest (A, B, i, j + ((n-j)/2), k)
		
		elif smallest_index_thusfar > k:
			print "C"
			return findKthSmallest (A, B, lowerme(i), j, k)

	elif A[i] < B[j]:
		if i + j + 2 == k:
			print "D"
			return B[j]
		
		elif i + j + 2 < k:
			print "E"
			return findKthSmallest (A, B, i + i/2, j, k)
		
		elif i + j + 2 > k:
			print "F"
			return findKthSmallest (A, B, i, lowerme(j), k)

	elif A[i] == B[j]:
		if smallest_index_thusfar == k:
			return A[i] #Found it!
		elif smallest_index_thusfar < k:
			#take smallest value and increase it
			if changeA(A,B,i,j,k,smallest_index_thusfar):
				return findKthSmallest(A,B,i + ((m-i)/2), j, k)
			else:
				return findKthSmallest(A,B,i, j + ((n-j)/2), k)
			
		elif smallest_index_thusfar > k:
			if changeA(A,B,i,j,k,smallest_index_thusfar):
				return findKthSmallest(A,B,lowerme(i),j,k)
			else:
				return findKthSmallest(A,B,i,lowerme(j),k)
			#take the largest value and decrease it
			
			
def changeA(A,B,i,j,k,sit):
	''' Tells you which one of the 
	two things A[i] or B[j] to modify '''

	if sit < k: #Select smallest
		ii = i + ((m-i)/2)
		jj = j + ((n-j)/2)
		if A[ii] < B[jj]:
			return True
		elif A[ii] > B[jj]:
			return False
		elif A[ii] == B[jj]:
			return changeA(A,B,ii,jj,k, ii + jj + 2)

	elif sit > k: #select largest
		ii = lowerme(i)
		jj = lowerme(j)
		if A[ii] < B[jj]:
			return False
		elif A[ii] > B[jj]:
			return True
		elif A[ii] == B[jj]:
			return changeA(A,B,ii,jj,k, ii + jj + 2)

	else:
		print "ERROR!!!!"


print findKthSmallest(A, B, i, j, 7)



