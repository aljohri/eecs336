S = [-1, -2, 5, 6, -10]
memo = [0, 0, 0, 0, 0, 0]

def maxsum(i):
	currmax = S[i]
	curr = 0
	for k in range(i+1, len(S), 1):
		curr = curr + memo[k]
		if curr > currmax:
			currmax = curr
	return currmax

def seqmaxsum(S):
	n = len(S) - 1
	memo[n + 1] = 0
	for i in range(n,1, -1):
		memo[i] = max(maxsum(i), memo[i + 1])
	return memo