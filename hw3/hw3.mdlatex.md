# Algorithms Homework 3 

@(Algorithms)[]

http://ttic.uchicago.edu/~shili/courses/EECS336/PSs/PS3.pdf

## Problem 1

>Johnson's algorithm

>The idea of Johnson’s algorithm is to re-weight all edges to make them positive via the Bellman-Ford algorithm, and then apply Dijkstra’s algorithm for every vertex.

>1. Add node q to the graph, connected by zero weight edges to the other nodes.
>2. Use Bellman-Ford algorithm starting from node q, to find for each vertex the minimum weight of a path from q to v. If this step detects a negative cycle, the algorithm is terminated.
>3. The edges for the original graph are reweighted using the values computed by the Bellman-Ford algorithm. An edge from u to v, having length $w(u,v)$ is given the new length $w_a(u,v) = w(u,v) + a(u) - a(v)$
>4. Remove node q and use Djikstra's algorithm to find the shortest paths from each node to each other vertex in the reweighted graph.

**Part A.** *Prove that for any u, v ∈ V , any shortest path from u to v in the weighted graph (G, w) is also a shortest path from u to v in the weighted graph (G, w').*

The algorithm for this problem is referred to as Johnson's algorithm. Essentially, use Bellman-Ford algorithm to transform the input graph into a graph without negative weights. Then, use Djikstra's algorithm on the transformed graph. 

Reweighting does not change shortest paths. Given a weighted, directed graph G = (V,E) with weight function $w : E \rightarrow \mathbb{R}$, let $h : V \rightarrow \mathbb{R}$ be any function mapping verticies to real numbers. For each edge $(u,v) \in E$, define $\hat{w}(u,v) = W(u,v) + h(u) - h(v)$.

<!--
https://www.cise.ufl.edu/class/cot5405sp11/slides/ch25.pdf
-->

Let $p = <v_0, v_1, ...., v_k$ be any path from vertex $v_0$ to vertex $v_k$. Then p is a shortest path from $v_0$ to  $v_k$ with weight function w if and only if it is a shortest path with weight function $\hat{w}$. That is, $w(p) = \gamma(v_0, v_k)$ if and only if $\hat{w}(p) = \hat{\gamma}(v_0, v_k)$. Furthermore, G has a negative weight cycle using weight function w if and only if G has a negative-weight cycle using weight function $\hat{w}$.

$$
\begin{align*}
\text{First we prove } \hat{w}(p) & = w(p) + h(v_0) - h(v_k) \\
\hat{w}(p) &= \sum\limits_{i=1}^k \hat{w}(v_{i-1}, v_i) \\
& = \sum\limits_{i=1}^k (w(v_{i-1}, v_i) + h(v_{i-1}) - h(v_i)) \\
& = \sum\limits_{i=1}^k w(v_{i-1}, v_i) + h(v_0) - h(v_k) \\
& = w(p) + h(v_0) - h(v_k) \\
\\
\text{With cycle } c &= <v_0, v_1, ..., v_k>, v_0 = v_k \\
\hat{w}(c) & = w(c) + h(v_0) - h(v_k)
& = w(c)
\end{align*}
$$

**Part B.** *Show that if (G, w) does not contain a negative cycle, then there is a function a such that $w_a(u, v) \ge 0$ for every (u, v) ∈ E.*

- If a cycle exists in G consisting of vertices $V = {v_1, v_2,...v_n}$. 
- The cycle consists of edges $E = {(v_1,v_2), (v_2,v_3), (v_3,v_4), ..., (v_n,v_n), (v_n,v1)}$. 
- If a negative cycle exists, the sum of weights $w = {w(v1,v1), ..., w(v_n,v_n), w(v_n,v_1)} \ge 0$.
- Let $a$ be a function calculating the shortest path from a vertex $v*$ to the given node. Because not all nodes can be reached by a path, we add an external vertex $V*$ to $V$, with edges drawn to every other vertex in V, each with weight 0. Therefore, the min path length from $V*$ to any node in the graph is of most 0. By the triangle inequality, we can show that  $d(v*,v) <= d(v*,u) + w(u,v)$

  $$
  \begin{align*}
  w_a(u,v) & = w(u,v) + a(u) - a(v) \\
  w_a(u,v) & = w(u,v) + d(v*,u) - d(v*,v)
  \end{align*}
  $$

  Since $w(u,v) + d(v*,u) \ge d(v*,v)$ the weight of all edges $w_a(u,v)$ will always be $\ge$ 0.

## Problem 2

**Part A.** *Problem 15-2 on Page 405 of the textbook (longest palindrome subsequence).*

Trivial Case:
	- empty string
	- single character

For the substring x[i,...,j]: 
- if x[i]==x[j], we can say that the length of the longest palindrome is the longest palindrome over x[i+1,...,j-1]+2.
- If they don't match, the longest palindrome is the maximum of that of x[i+1,...,j] and y[i,...,j-1].

<!--
http://ajeetsingh.org/2013/11/12/find-longest-palindrome-sub-sequence-in-a-string/
-->

```python
def lps(str, i, j):
	# Base Case 1: If there is only 1 character
	if i == j: return 1
	
	# Base Case 2: If there are only 2 characters and both are same
	if str[i] == str[j] and i+1 == j:
		return 2
	
	# If the first and last characters match
	if str[i] == str[j]:
		return lps(str, i+1, j-1) + 2
	
	# If the first and last characters do not match
	return max(lps(str, i, j-1), lps(str,i+1,j))
```

One can use memoization on this algorithm to reduce needless computation and lower the time complexity.

```python
def lps(str):
	n = len(str)
	LP = [[0 for i in range(n)] for j in range(n)]
	
	# All substrings with 1 character will be a palindrome of size 1
	for i in xrange(n): LP[i][i] = 1

	# Gap is between i, j
	for gap in xrange(n):
		for i in xrange(n-gap):
			j = i + gap
			if str[i] == str[j] and gap == 1:
				LP[i][j] = 2
			elif str[i] == str[j]:
				LP[i][j] = LP[i+1][j-1] + 2
			else:
				LP[i][j] = max(LP[i][j-1], LP[i+1][j])
	return LP[0][n-1]
```

**Part B.** *Suppose the goal of the problem is to compute the length of the longest palindrome (you do not need to give a longest palindrome). Give a $O(n^2)$-time $O(n)$ space algorithm that computes the length of the longest palindrome. (n is the length of the input string.)*

Start from the left-most position. For each position, keep track of a table of the farthest out points at which you can generate reflected subsequences of length 1, 2, 3, etc. (Meaning that a subsequence to the left of our point is reflected to the right.) For each reflected subsequence we store a pointer to the next part of the subsequence.

As we work our way right, we search from the RHS of the string to the position for any occurrences of the current element, and try to use those matches to improve the bounds we previously had. When we finish, we look at the longest mirrored subsequence and we can easily construct the best palindrome.

## Problem 3

http://math.stackexchange.com/questions/187555/shortest-paths-from-s-by-weight-which-contain-even-number-of-edges

*Let G = (V, E) be a directed acyclic graph. Assume V = {1, 2, 3, · · · , n} and all edges (i, j) ∈ E has i < j. Let w : E → R be a weight function. We say a path is an even path if it contains an even number of edges. Give a O(E) time algorithm that computes the shortest even path from 1 to n.*

1. Create a new graph with the vertices being $V \cup V'$ where $V'$ is a replicate of the vertices of G (meaning $|V'| = |V|$ and the sets are disjoint).
  The edges are:
  $$e' = (i,j') \in E' \leftrightarrow e = (i,j) \in E$$
  $$e' = (i',j) \in E' \leftrightarrow e = (i,j) \in E$$
  (there are no edges of the form $(i,j), (i',j')$).
  The weights are $w'(i,j') = w'(i',j) = w((i,j)$.
2. Now, any path from i to j must be of even length (paths of odd length end up in $V'$ and not in V).
3. Now you can use Bellman-Ford.

## Problem 4

Perform a depth first search. If the input node has no subnodes then return, 

```python
T_prime = []
def max_weight_subtree(T):
	T = []
	A = []
	if T.children() == None:
		return (w(T), T)
	else:
		for child in T.children():
			A += max_weight_subtree(child)
		B += A.pop_max_weight()
		B += A.pop_max_weight()
		n_copy = T.current_node()
		n_copy.weight = w(T) + B[0].weight + B[1].weight
		n_copy.left = B[0]
		n_copy.right = B[1]
		T_prime += n_copy
		return n_copy
```

It's correct because it creates a binary tree of the maximum weight subgraph. Each level takes the two maximum nodes and uses dynamic programming. 